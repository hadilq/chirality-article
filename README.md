
# Generalization of Lipkin's zilch to $n$-dimensional Minkowski space-time

## Abstract

As Noether's theorem states any differentiable symmetry of the action of a physical system has a 
corresponding conservation law. Lipkin introduced zilches and the conservation laws governing them. 
With the recent growing interest in optical chirality, which is a zilch, efforts to gain further 
insight into symmetries underlying the conservation of zilches have grown. Here we generalize zilches 
to $n$-dimensional Minkowski space-time and find the related generalized symmetry transformations. 
This generalization is inspired in part by recent surging interest in using extra dimensions to 
solve different problems, including the Hirarchy problem of \textit{Standard Model}. 
Here, we demonstrate that for $n>4$ Zilches in general and Optical Chirality in particular, are not conserved. 
This result may be of significance in examining the physics in higher dimensions.  
The fact that this finding lies in the context of electromagnetism and departs from the mainly particle 
physics scenarios makes it distinct.

## Conclusion

In this paper we generalized zilches to $n$-dimensional Minkowski space-time and obtained the 
corresponding generalized symmetry transformations. As the major finding of this paper we demonstrated 
that for $n>4$, even in lack of current-density, creation and annihilation of zilches can occur. 
Same holds true for optical chirality which is a Zilch. This novel insight adds to our fundamental 
understanding of optical chirality and the continuity equation governing its density and flux. 
Also, this finding can be of significance in experimental explorations of electromagnetism in $n>4$ space-time.

## This is a green open access article

As science, just like economics, needs freedom to exist, I encourage all researchers to publish their research 
in git repositories, because this technology is so suitable to keep track of the time events, which is exactly 
what science is needed to flourish.
