\RequirePackage{amsmath}
\documentclass[njp,12pt,preprint]{iopart}
\usepackage{amssymb}
\usepackage{etoolbox}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{blindtext}

\AtBeginEnvironment{align}{\setcounter{subeqn}{0}}% Reset subequation number at start of align
\newcounter{subeqn} \renewcommand{\thesubeqn}{\theequation\alph{subeqn}}%
\newcommand{\subeqn}{%
  \refstepcounter{subeqn}% Step subequation number
  \tag{\thesubeqn}% Label equation
}

\usepackage{appendix}


\begin{document}

\title{Generalization of Lipkin's zilch to $n$-dimensional Minkowski space-time}
\author{H Lashkari-Ghouchani$^1$, M H Alizadeh$^{2,3}$\footnote{Author to whom any correspondence should be addressed.}}
\address{$^1$ Faculty of Science, PNUM. Mashhad, Iran.}
\address{$^2$ Physics Department, Boston University, Boston, MA 02215}
\address{$^3$ Photonic Center, Boston University, Boston, MA 02215}
\ead{\href{mailto:halizade@bu.edu}{halizade@bu.edu}}

\begin{abstract}
As Noether's theorem states any differentiable symmetry of the action of a physical system has a corresponding conservation law. Lipkin introduced zilches and the conservation laws governing them. With the recent growing interest in optical chirality, which is a zilch, efforts to gain further insight into symmetries underlying the conservation of zilches have grown. Here we generalize zilches to $n$-dimensional Minkowski space-time and find the related generalized symmetry transformations. This generalization is inspired in part by recent surging interest in using extra dimensions to solve different problems, including the Hirarchy problem of \textit{Standard Model}. Here, we demonstrate that for $n>4$ Zilches in general and Optical Chirality in particular, are not conserved. This result may be of significance in examining the physics in higher dimensions.  The fact that this finding lies in the context of electromagnetism and departs from the mainly particle physics scenarios makes it distinct.
\end{abstract}
\noindent{\it Keywords\/}: energy-momentum, chirality, zilch, symmetry, $n$-Minkowski

\maketitle

\section{Introduction}
As Lipkin proved, there are ten conserved quantities for four dimensional electromagnetic fields in vacuum which are shown to be independent of energy-momentum\cite{lipkin1964existence}. He clarified that the new conserved quantities possess different flow properties compared to energy-momentum\cite{lipkin1964existence}. Originally, Lipkin called those conserved quantities \textit{zilch}, the most renown of which is $00$-zilch, $z^{00}$, the \textit{optical chirality}. Optical chirality of an electromagnetic field is a measure of its interaction with chiral matter. The pioneering work by Tang and Cohen showed that optical chirality can be enhanced locally through interference between electromagnetic fields \cite{tang2011enhanced}. Soon after, people realized that localized plasmons can be utilized for enhancement of optical chirality \cite{henry2010nano,alizadeh2015plasmonically,alizadeh2015enhanced}. Such enhancement schemes that lead to enhanced chiral light-matter interactions are highly desirable in many practical fields including pharmaceutical applications. Despite all the focus on practical implementations of optical chirality, little work has been done on its fundamental understanding. Lipkin did not find the responsible symmetries for the conservation of the zilches. Recently there has been a growing interest in gaining further insight into these symmetries. For instance, an approach to discuss these symmetries and their connection with helicity's symmetry is presented in \cite{cameron2012optical}. In \cite{philbin2013lipkin} Philbin finds the symmetries that are restricted on optical chirality. Also, Bliokh and Nori clarified the connection between helicity, optical chirality and energy which explains why we will find a symmetry of zilches similar to that of helicity\cite{cameron2012optical,bliokh2011characterizing,heaviside1892forces,larmor1897dynamical}. Additionally, Cameron and Barnett discuss the symmetries of helicity and zilches for $n=4$ in \cite{cameron2012electric}. \\
We extend recent efforts in determining the underlying symmetries of zilch conservation to $n$-dimensional Minkowski space-time. This generalization is inspired in part by recent interest in using higher dimensions to solve some long-standing problems in Physics. For instance, introducing an extra non-compactified lengthlike dimension has been utilized\cite{visser1999exotic,chodos1980has}. Then we apply these symmetries to the action of standard electromagnetic fields, $\frac{1}{16\pi} F_{\alpha\beta}F^{\alpha\beta}$, to derive the conservation laws of zilches for generic $n$ and elaborate on their connection with current-density. Through such generalization we show that zilches are not conserved in $n>4$ Minkowski space-time. The fact that this finding lies in the context of electromagnetism and departs from the particle physics scenarios may open up new possiblities in testing these results through optical experiments. In doing so and in order to simplify our calculations we use linear form notation. This dramatically reduces our dependence on indices. The organization of this paper is as follows: in the first part we introduce Lagrangian $n$-form and go on to derive zilches conservation in linear form notation and then the underlying symmetries are obtained. Then we show that in the special case of $n=4$, the continuity equation governing optical chirality density and flow is obtained. The first Appendix is dedicated to derive the conservation of energy-momentum for general $n$. The second Appendix contains proofs of the identities used in the main text. \\


\section{Lagrangian $n$-form}
As a reminder of linear form machinery, here we derive Maxwell's equations from standard electromagnetic Lagrangian by linear forms. Consider an $n$-dimensional Minkowski space-time with $x^\mu$ coordinates. In general we can write an m-form by
\begin{equation}
a^m=a_{\mu_1\mu_2\ldots\mu_m}dx^{\mu_1}\wedge dx^{\mu_2}\wedge \ldots \wedge dx^{\mu_m}\nonumber
\end{equation}
or more briefly by
\begin{equation}\label{formdef}
a^m=a_{\mu^m}dx^{\mu^m}
\end{equation}
where $\wedge$ is the \textit{exterior product} or \textit{wedge product}, which makes a completely antisymmetric tensor\cite{frankel2004geometry}. One must be careful about the upper and lower index of indices. The lower index of indices denotes the number of that index, for example $\mu_m$ is the $m$th index, while the upper index of indices indicates the counting indices of a set, for instance, $\mu^m=\mu_1\mu_2\ldots\mu_m$. If we carefully look at \ref{formdef} we will notice that if $a_{\mu^m}$ is a complete antisymmetric tensor then every term in \ref{formdef} is repeated $m!$ times, where $!$ is the factorial. To avoid this repetition we use an arrow, $a_{\underrightarrow{\mu^m}}$, to assume $\mu_1<\mu_2<\ldots<\mu_m$ such that
\begin{equation}
a^m=\frac{1}{m!}a_{\mu^m}dx^{\mu^m}=a_{\underrightarrow{\mu^m}}dx^{\mu^m}.\nonumber
\end{equation}
So by $n$-potential $A=A_\beta dx^\beta$ we define the electromagnetic 2-form as follows:
\begin{equation}\label{fielddef}
F=F_{\underrightarrow{\mu^2}} dx^{\mu^2}=\frac{1}{2}F_{\mu^2} dx^{\mu^2}
=dA=dA_\beta\wedge dx^\beta=\partial_\alpha A_\beta dx^\alpha\wedge dx^\beta.
\end{equation}
Where $d$ is \textit{exterior differentiation} and by this equation we mean $F_{\alpha\beta}=\partial_\alpha A_\beta-\partial_\beta A_\alpha$. The immediate consequence of this definition is
\begin{equation}\label{max1}
dF=0.
\end{equation}
To write Lagrangian and evaluate its variation, we need Hodge operator, $*$, \cite{frankel2004geometry}. We can define dual of an $m$-form, $a^m$, which is a $(n-m)$-form, $*a^m$, by
\begin{eqnarray}
*a^m
&:=a^*_{\underrightarrow{\mu^{n-m}}}dx^{\mu^{n-m}}
=\sqrt{|g|} a^{\nu^m}\epsilon_{\underrightarrow{\nu^m}\underrightarrow{\mu^{n-m}}}dx^{\mu^{n-m}}\nonumber\\
&=a_{\alpha^m}g^{\alpha_1\nu_1}g^{\alpha_2\nu_2}\dots g^{\alpha_m\nu_m}\epsilon_{\underrightarrow{\nu^m}\underrightarrow{\mu^{n-m}}}dx^{\mu^{n-m}}
\end{eqnarray}
where $g$ is the determinant of the metric in $n$-Minkowski space-time, $g=det(g_{\alpha\beta})=-1$, $g^{\alpha\beta}$ is inverse of the metric tensor and $\epsilon_{\beta^n}$ is the Levi-Civita symbol, which is completely antisymmetric and $\epsilon_{123...n}=1$. For the important case of 0-form constant function, $f=1$, we can define $vol^n:=\sqrt{|g|}dx^{12...n}=*1$. We will then have
\begin{equation}\label{starreplace}
\langle a^m,b^m \rangle vol^n := a_{\underrightarrow{\mu^m}}b^{\mu^m} vol^n
=a^m\wedge*b^m = b^m\wedge*a^m.
\end{equation}
also one can easily show
\begin{equation}\label{starstar}
*(*a^m)=-(-1)^{m(n-m)}a^m.
\end{equation}
Now that the necessary definitions are made we can proceed to write the action:
\begin{equation}
S:=\int_M \mathcal{L}=\int_M \frac{1}{8\pi} F\wedge *F-A\wedge *J
\end{equation}
which $\mathcal{L}$ is the Lagrangian $n$-form, $J$ is the $n$-current-density and the integrals are defined over the space-time $M$. Using the variation $A\rightarrow A+\delta A$ and \ref{fielddef}, we can find the equation of motions\cite{jose1998classical,weinberg1996quantum}.
\begin{equation}\label{eqmotion}
*d*F=4\pi{(-1)}^nJ.
\end{equation}
\ref{max1} and \ref{eqmotion} are the \textit{Maxwell's equations}. Conservation of electric charge can be found by:
$
*J=\frac{1}{4\pi}d*F
$
so
$
d*J=0
$
which means electric charge that passes through region $V^{n-1}$, $ 4\pi q=\int_V 4\pi *J=\int_V d*F=\int_{\partial V} *F$, is a conserved quantity. It is worth mentioning that we did not use any indices to calculate equations of motions and conservation equations. We will express symmetries of this action and their conserved quantities in this powerful indexless notation. One can expand equations of motion, \ref{eqmotion}, to find
\begin{equation}\label{JF}
\partial_\beta F^{\alpha\beta}=4\pi J^\alpha
\end{equation}
detailed derivation can be found in the Appendix, \ref{JFproof}.

\section{Zilch}
Starting from zilch tensor as Lipkin defined\cite{lipkin1964existence} for $n=4$, one can generalize the zilch tensor to $n$-dimensional Minkowski space-time by the following variation
\begin{equation}\label{chvar}
F\rightarrow F+l^{\alpha\beta}*(N^{n-4}\wedge \partial_\alpha \partial_\beta F)
\end{equation}
where $N^{n-4}$ is a constant $(n-4)$-form, and $l^{\alpha\beta} $ are infinitesimal parameters of the variation. It is important to note that, because of $N$, which is a $(n-4)$-form, \ref{chvar} is defined only for $n\geq 4$. For $n=4$, this variation reduces to
\begin{eqnarray}
\mathbf{E}&\rightarrow \mathbf{E}-l^{\alpha\beta} \partial_\alpha \partial_\beta \mathbf{B}\nonumber\\
\mathbf{B}&\rightarrow \mathbf{B}+l^{\alpha\beta} \partial_\alpha \partial_\beta \mathbf{E}.\nonumber
\end{eqnarray}
Focusing on optical chirality, for a \textit{monochromatic} field this variation becomes
\begin{eqnarray}
&\mathbf{E}\rightarrow \mathbf{E}+l^{00} \omega^2 \mathbf{B}=\mathbf{E}+\theta \mathbf{B}\nonumber\\
&\mathbf{B}\rightarrow \mathbf{B}-l^{00} \omega^2 \mathbf{E}=\mathbf{B}-\theta \mathbf{E}\label{helsym}
\end{eqnarray}
where $\mathbf{E}$ is the electric and $\mathbf{B}$ the magnetic vector in cgs units, $\omega$ is the angular frequency, $\theta$ is an infinitesimal parameter of electric-magnetic rotation and \ref{helsym} is the infinitesimal form of the \textit{dual symmetry} or \textit{duplex transformation}\cite{bliokh2013dual,barnett2012duplex,cameron2012optical}. The dual symmetry is associated with optical helicity so the connection between special case of zilches, optical chirality, and helicity unfolds\cite{bliokh2011characterizing}. \\
In this stage we implement variation \ref{chvar} to calculate zilch tensor. Since zilch tensor has two distinct terms that each make a closed form of the variation of Lagrangian $n$-form, we go on to find these closed forms separately. The first closed form can be obtained using the following identity.
\begin{equation}
*\Big(N\wedge d*(* \partial_\alpha F\wedge g_{\beta\lambda}dx^\lambda)\Big)
=-*(N\wedge \partial_\alpha\partial_\beta F)\label{chfiridentity}
\end{equation}
the proof of this identity can be found in the Appendix, \ref{chfiridentity0}. Replacing $*(N\wedge \partial_\alpha\partial_\beta F)$ into the variation \ref{chvar} we achieve
\begin{equation}\label{chvarcomfirst}
F\rightarrow F-l^{\alpha\beta}g_{\beta\gamma}*\Big(N\wedge d*(*\partial_\alpha F\wedge dx^\gamma)\Big).
\end{equation}
So by applying \ref{max1}, \ref{starreplace}, \ref{starstar} the variation of the action would be
\begin{eqnarray}\label{chactionvar}
\delta S&=\frac{1}{4\pi}l^{\alpha\beta} g_{\beta\gamma}\int_M F\wedge N\wedge d*(*\partial_\alpha F\wedge dx^\gamma)\nonumber\\
&=\frac{1}{4\pi}l^{\alpha\beta} g_{\beta\gamma}\int_M d\big(F\wedge *(*\partial_\alpha F\wedge dx^\gamma)\wedge N\big).
\end{eqnarray}
This is the first closed form of the variation of Lagrangian $n$-form. In order to find the second closed form of the variation of Lagrangian $n$-form we use the following indentity, proof of which can be found in the Appendix, \ref{chsecidentity0}.
\begin{eqnarray}\label{chsecidentity}
g_{\beta\gamma}d*(N\wedge \partial_\alpha F\wedge dx^\gamma)=&*(N\wedge \partial_\alpha \partial_\beta F)\nonumber\\
&+ 4\pi g_{\beta\gamma}*(N\wedge dx^\gamma\wedge \partial_\alpha J)-U_{\alpha\beta}
\end{eqnarray}
where we have
\begin{equation}\label{ugly}
U_{\alpha\beta}:=(n-4)\epsilon_{\underrightarrow{\mu^{n-5}}\underrightarrow{\nu^2}\underrightarrow{\gamma^2}\beta}\partial_\alpha\partial_{\theta} F^{\nu^2} N^{\mu^{n-5}\theta}dx^{\gamma^2}.
\end{equation}
This term is of great significance. This is because it neither makes a closed form, nor is it related to the $n$-current-density. After similar procedure as \ref{chvarcomfirst} and using \ref{chsecidentity}, the variation of the action would be
\begin{eqnarray}
\delta S=\frac{1}{4\pi}l^{\alpha\beta} g_{\beta\gamma}
\int_M &\Big(d*(N\wedge \partial_\alpha F\wedge dx^\gamma)
\nonumber\\
&-4\pi *(N\wedge dx^\gamma\wedge \partial_\alpha J)+U_{\alpha}^{\ \gamma}\Big)\wedge *F.
\end{eqnarray}
For the sake of simplicity, here we assume there is no $n$-current-density, $J=0$, and $U_{\alpha}^{\ \gamma}=0$, going through the same procedure as in \ref{chactionvar}, we can make a closed form inside this integral. Subtracting these two integrals we will obtain an integral similar to $\int_M dZ_{\alpha}^{\ \gamma}=0$, where $Z_{\alpha}^{\ \gamma}$ is the zilch tensor. So based on Noether's theorem there is a conserved quantity\cite{jose1998classical}. Now, keeping the $n$-current-density and $U_{\alpha}^{\ \gamma}$, the general equation will be
\begin{eqnarray}
dZ_{\alpha}^{\ \gamma}&=d\Big(F\wedge *(*\partial_\alpha F\wedge dx^\gamma)\wedge N
-*(N\wedge\partial_\alpha F \wedge dx^\gamma)\wedge *F\Big)\refstepcounter{equation}\label{chconservation}\\
&=4\pi N\wedge\Big(J\wedge \partial_\alpha F-\partial_\alpha J\wedge F\Big)\wedge dx^\gamma
+U_{\alpha}^{\ \gamma}\wedge *F\label{currentofch}.
\end{eqnarray}
This identity defines \textit{zilch tensor} in $n$-Minkowski space-time and its relation to $n$-current-density. If all terms of \ref {currentofch} go to zero, then there is a conserved quantity, $z_{\alpha}^{\ \gamma}=\int_V Z_{\alpha}^{\ \gamma}$, that passes through region $V^{n-1}$ which Lipkin named \textit{zilch}. To see why this term is Lipkin's zilch tensor we need to expand it. For the first term of $Z_{\alpha}^{\ \gamma}$ in \ref{chconservation} this procedure is less cumbersome if we multiply it by $dx^\phi$
\begin{eqnarray}
dx^\phi\wedge F\wedge &*(g^{\alpha\gamma}*\partial_\gamma F\wedge dx^\beta)\wedge N\nonumber\\
&=g^{\alpha\gamma}g^{\beta\sigma} \epsilon^{\phi\nu^2\lambda\mu^{n-4}}F_{\underrightarrow{\nu^2}}\partial_\gamma F_{\lambda\sigma}N_{\underrightarrow{\mu^{n-4}}}vol^n.
\end{eqnarray}
Also, the second term of $Z_{\alpha}^{\ \gamma}$ in \ref{chconservation} can be expanded as follows
\begin{eqnarray}
-dx^\phi \wedge *(N\wedge g^{\alpha\gamma}\partial_\gamma F \wedge dx^\beta)\wedge *F
&=-g^{\alpha\gamma_1}g^{\beta\gamma_2}\epsilon^{\phi\kappa\theta^{n-2}}\epsilon_{\underrightarrow{\mu^{n-4}}\underrightarrow{\nu^2}\gamma_2\kappa}\nonumber\\
&\times\epsilon_{\underrightarrow{\sigma^2}\underrightarrow{\theta^{n-2}}}
N^{\mu^{n-4}}\partial_{\gamma_1}(F^{\nu^2})F^{\sigma^2} vol^n\nonumber\\
=-det(g^{\sigma\theta})g^{\alpha\gamma}g^{\phi\psi}\epsilon^{\overrightarrow{\mu^{n-4}}\overrightarrow{\nu^2}\beta\kappa}N_{\mu^{n-4}}
&\partial_{\gamma}(F_{\nu^2})F_{\psi\kappa} vol^n.
\end{eqnarray}
In case of $n=4$ zilch tensor becomes
\begin{eqnarray}
Z^{\alpha\beta\phi}vol^4&:=dx^\phi \wedge Z^{\alpha\beta}\nonumber\\
&=N g^{\alpha\gamma}\Big(g^{\phi\psi}\epsilon^{\nu^2\beta\kappa} \partial_{\gamma}(F_{\underrightarrow{\nu^2}})F_{\psi\kappa}
+g^{\beta\sigma} \epsilon^{\phi\nu^2\lambda}F_{\underrightarrow{\nu^2}}\partial_\gamma F_{\lambda\sigma}\Big)vol^4 \nonumber
\end{eqnarray}
in which case $N$ is a number. If $N=1$ this expression is equal to the first four terms of original definition of Lipkin's zilch tensor, equation (4) of original Lipkin's paper\cite[p. 2]{lipkin1964existence}. Care must be taken in treating \ref{chfiridentity} and \ref{chsecidentity}, which are in 4-Minkowski space-time. This is because in the absence of 4-current-density they are symmetric upon exchange of $\alpha\beta$. Additionally, one can show the four last terms in the original Lipkin's definition\cite[p. 2]{lipkin1964existence} make a closed form, therefore, they can be removed from the definition.\\
If we expand the last term of \ref{currentofch} we will have
\begin{equation}\label{uglycurrent}
U_{\alpha\beta}\wedge*F
=(n-4)N^{\mu^{n-5}\theta}\epsilon_{\underrightarrow{\mu^{n-5}}\underrightarrow{\gamma^2}\underrightarrow{\nu^2}\beta}F^{\gamma^2}\partial_\theta\partial_\alpha F^{\nu^2}vol^n.
\end{equation}
It is noted that for $n=4$, $U_{\alpha\beta}=0$ and \ref{uglycurrent} can be disregarded for $n=4$ theories. However for $n>4$ and $J=0$, \ref{uglycurrent} predicts generation and annihilation of zilches inside a region, which lacks in $n=4$ theories. This is the main result of this paper.\\
As the final remark we derive the governing equation for optical chirality in $n=4$ from the general formulas \ref{chconservation}-\ref{currentofch}. One can easily show that in the case of $n=4$ \ref{chconservation}-\ref{currentofch} reduces to
\begin{equation}\label{optchira}
\partial_t\big(\mathbf{B}.\partial_t\mathbf{D}-\mathbf{D}.\partial_t\mathbf{B}\big)+\nabla.\big(\mathbf{E}\times\partial_t\mathbf{D}+\mathbf{H}\times\partial_t\mathbf{B}\big)
=4\pi (\mathbf{J}.\partial_t\mathbf{B}-\mathbf{B}.\partial_t\mathbf{J})
\end{equation}
where $\mathbf{D}$ is the electric displacement field and $\mathbf{H}$ is the magnetizing field. $\mathbf{B}.\partial_t\mathbf{D}-\mathbf{D}.\partial_t\mathbf{B}$ is the \textit{optical chirality density} and $\mathbf{E}\times\partial_t\mathbf{D}+\mathbf{H}\times\partial_t\mathbf{B}$ is the \textit{optical chirality flow}\cite{tang2010optical}. In the case of $\mathbf{J}=0$ we get the continuity equation of optical chirality
\begin{equation}\label{tang}
\partial_t\big(\mathbf{B}.\nabla\times\mathbf{H}+\mathbf{D}.\nabla\times\mathbf{E}\big)
+\nabla.\big(\mathbf{E}\times\nabla\times\mathbf{H}-\mathbf{H}\times\nabla\times\mathbf{E}\big)=0.
\end{equation}
For electromagnetic field in vacuum, \ref{optchira} and \ref{tang} can interchangeably be used to denote optical chirality density and optical chirality flow. In case of non-zero current density, however, extra care must be taken as which definition to use.


\section{Conclusion}
In this paper we generalized zilches to $n$-dimensional Minkowski space-time and obtained the corresponding generalized symmetry transformations. As the major finding of this paper we demonstrated that for $n>4$, even in lack of current-density, creation and annihilation of zilches can occur. Same holds true for optical chirality which is a Zilch. This novel insight adds to our fundamental understanding of optical chirality and the continuity equation governing its density and flux. Also, this finding can be of significance in experimental explorations of electromagnetism in $n>4$ space-time.


\section*{References}
\bibliographystyle{iopart-num}

\bibliography{chirality}

\appendix

\section{Energy-momentum}
Energy-momentum tensor of an electromagnetic field can be written as:
\begin{equation}\label{energym}
T_{\alpha\beta}=F_{\alpha\gamma}F^{\ \ \gamma}_{\beta}-\frac{1}{4}g_{\alpha\beta}F_{\mu\nu}F^{\mu\nu}.
\end{equation}
In order to demonstrate the advantage of linear form machinery and in order to expand the method we used for zilches, in this part we derive electromagnetic energy-momentum tensor in linear forms. Through doing so, we show that the appearance of indices is not necessary and one does not have to manually insert divergent-free terms in the calculations. Also, the gauge invariance of energy-momentum is automatically fulfilled. we start from the infinitesimal variation of $h_\alpha$ over space-time, which is this transformation
\begin{equation}\label{varform}
F\rightarrow F+h^\alpha\partial_\alpha F.
\end{equation}
Here, just like the zilch section we use two identities to make the variation of Lagrangian $n$-form a closed form. The first identity is as follows
\begin{equation}\label{firidentity}
d*(* F\wedge g_{\beta\lambda}dx^\lambda)
=-\partial_\beta F
\end{equation}
details of derivation can be found in Appendix, \ref{engfirstidentity}. The next one, \ref{engsecidentity}, is
\begin{equation}\label{secidentity}
*d*(F\wedge dx^\beta)=4\pi J\wedge dx^\beta-\partial_\alpha F g^{\alpha\beta}.
\end{equation}
Similar to what we did in the zilch section, we use \ref{max1}, \ref{starreplace}, \ref{starstar}, \ref{eqmotion} and assuming no $n$-current-density, $J^\alpha=0$, to find $\int_M dT^\alpha=0$. Based on Noether's theorem we have $n$ conserved quantities, $p^\alpha=\int_V T^\alpha$, that pass through region $V^{n-1}$\cite{jose1998classical,weinberg1996quantum}. These $n$ conserved quantities are the $n$-vector of energy-momentum of the field. In the general case when $n$-current-density, $J$, is non-zero we have
\begin{eqnarray}\label{emconservation}
dT^\alpha&=-\frac{1}{2}d\Big(*(*F\wedge dx^\alpha)\wedge*F+F\wedge *(F\wedge dx^\alpha)\Big)\nonumber\\
&=4\pi J\wedge dx^\alpha\wedge * F
\end{eqnarray}
where $T^\alpha$ denotes the \textit{energy-momentum tensor}, the components of which can be shown to be the same as \ref{energym}, see the Appendix, \ref{energym0}. The simplicity of the proof and the fact that we did not have to go through Belinfante symmetrization procedure for the energy-momentum tensor add to the power of our method. Some other approaches can be found here\cite{belinfante1940quantum,bliokh2013dual,jose1998classical}. Further, the obtained tensor is naturally gauge invariant. We mention in passing that the conservation of angular momentum can be addressed by replacing transformation operator, $h^\alpha \partial_\alpha$, in \ref{varform} by rotation operator, $ \omega^{\alpha\beta}(x_\alpha\partial_\beta-x_\beta\partial_\alpha)$, so the variation of electromagnetic field for infinitesimal rotation is
\begin{equation}
F\rightarrow F+\omega^{\alpha\beta}(x_\alpha\partial_\beta-x_\beta\partial_\alpha) F.\nonumber
\end{equation}
Going through the same procedure the angular momentum tensor of the electromagnetic field can be found to be
\begin{eqnarray}
dM^{\alpha\beta}
&=-\frac{1}{2}d\Big(&*\big(*F\wedge (x^\alpha dx^\beta-x^\beta dx^\alpha)\big)\wedge*F\nonumber\\
&&+F\wedge *\big(F\wedge (x^\alpha dx^\beta-x^\beta dx^\alpha)\big)\Big)\nonumber\\
&=4\pi J\wedge &(x^\alpha dx^\beta-x^\beta dx^\alpha)\wedge * F.\nonumber
\end{eqnarray}
which demonstrates the conservation of angular momentum in absence of $n$-vector of current-density, $dM^{\alpha\beta}=0$.


\section{Identities}
Here we derive and expand in detail the identities referred in the main text.
\begin{eqnarray}\label{calcexample1}
*d*F&=*d*\left(F_{\underrightarrow{\mu^2}}dx^{\mu^2}\right)=*d\left(F^{\mu^2}\epsilon_{\underrightarrow{\mu^2}\underrightarrow{\nu^{n-2}}}dx^{\nu^{n-2}}\right)\nonumber\\
&=det(g^{\sigma\theta})\epsilon^{\beta\nu^{n-2}\alpha}\epsilon_{\underrightarrow{\mu^2}\underrightarrow{\nu^{n-2}}}\partial_\beta F^{\mu^2}g_{\alpha\gamma}dx^\gamma\nonumber\\
&={(-1)}^n\frac{1}{2}(\delta^\alpha_{\mu_1}\delta^\beta_{\mu_2}-\delta^\alpha_{\mu_2}\delta^\beta_{\mu_1})\partial_\beta F^{\mu^2}g_{\alpha\gamma}dx^\gamma
={(-1)}^n\partial_\beta F^{\alpha\beta}g_{\alpha\gamma}dx^\gamma.
\end{eqnarray}
Based on \ref{eqmotion}, it must be equal to
$
4\pi{(-1)}^n J_\gamma dx^\gamma
$
so
\begin{equation}\label{JFproof}
\partial_\beta F^{\alpha\beta}=4\pi J^\alpha\qquad\qed
\end{equation}
In the first section,  zilch, we needed two identities. For the first identity, we try to expand $*\Big(N\wedge d*(* F\wedge g_{\beta\lambda}dx^\lambda)\Big)$. To do so we assume a free constant $(n-4)$-form, $N^{n-4}$ and use the following identity
\begin{equation}\label{antisymiden}
\epsilon_{[\mu^n}g_{\mu_{n+1}]\beta}=0
\end{equation}
which is because a term with $n+1$ completely antisymmetric indices, in $n$-dimensional space-time, is always zero. Now we can write
\begin{eqnarray}
*\Big(N\wedge d*(* F\wedge g_{\beta\lambda}dx^\lambda)\Big)
&=*\Big(N\wedge d(F_{\nu\beta}dx^\nu)\Big)\nonumber\\
&=\epsilon_{\underrightarrow{\mu^{n-4}}\lambda\nu_1\underrightarrow{\gamma^2}} g_{\nu_2\beta}\partial^\lambda  F^{\nu^2} N^{\mu^{n-4}}dx^{\gamma^2}\nonumber\\
&=-\epsilon_{\underrightarrow{\mu^{n-4}}\underrightarrow{\nu^2}\underrightarrow{\gamma^2}}g_{\lambda\beta}\partial^\lambda  F^{\nu^2} N^{\mu^{n-4}}dx^{\gamma^2}\nonumber\\
&=-*(N\wedge \partial_\beta F)\label{chfiridentity0}
\end{eqnarray}
where we used $\partial^{[\lambda}F^{\nu^2]}=0$, which is another version of $dF=0$. The second identity can be achieved by implementing a similar identity to \ref{antisymiden}, $\epsilon_{[\mu^n}\partial_{\mu_{n+1}]}=0$. Applying it to
\begin{eqnarray}
&g_{\beta\gamma}d*(N\wedge \partial_\alpha F\wedge dx^\gamma)
=\epsilon_{\underrightarrow{\mu^{n-4}}\underrightarrow{\nu^2}\beta\gamma}\partial_\sigma \partial_\alpha F^{\nu^2} N^{\mu^{n-4}}dx^\sigma \wedge dx^\gamma\nonumber
\end{eqnarray}
we can get
\begin{eqnarray}
&=\frac{1}{2}\Bigg(\epsilon_{\underrightarrow{\mu^{n-4}}\underrightarrow{\nu^2}\gamma^2}\partial_\beta&+2\epsilon_{\underrightarrow{\mu^{n-4}}\beta\underrightarrow{\gamma^2}\nu_1}\partial_{\nu_2}
-(n-4)\epsilon_{\underrightarrow{\mu^{n-5}}\underrightarrow{\nu^2}\beta\gamma^2}\partial_{\mu_{n-4}}\Bigg)\nonumber\\
&&\times\partial_\alpha F^{\nu^2} N^{\mu^{n-4}}dx^{\gamma^2}\nonumber\\
&=*(N\wedge \partial_\alpha \partial_\beta F)&+ 4\pi g_{\beta\gamma}*(N\wedge dx^\gamma\wedge \partial_\alpha J)\nonumber\\
&&-\frac{1}{2}(n-4)\epsilon_{\underrightarrow{\mu^{n-5}}\underrightarrow{\nu^2}\beta\gamma^2}\partial_{\theta}\partial_\alpha F^{\nu^2} N^{\mu^{n-5}\theta}dx^{\gamma^2}.\nonumber
\end{eqnarray}
Where we again have
$$
U_{\alpha\beta}:=(n-4)\epsilon_{\underrightarrow{\mu^{n-5}}\underrightarrow{\nu^2}\beta\underrightarrow{\gamma^2}}\partial_{\theta}\partial_\alpha F^{\nu^2} N^{\mu^{n-5}\theta}dx^{\gamma^2}.
$$
So the second identity is
\begin{eqnarray}
g_{\beta\gamma}&d*(N\wedge \partial_\alpha F\wedge dx^\gamma)=*(N\wedge \partial_\alpha \partial_\beta F)\nonumber\\
&+ 4\pi g_{\beta\gamma}*(N\wedge dx^\gamma\wedge \partial_\alpha J)-U_{\alpha\beta}\qed\label{chsecidentity0}
\end{eqnarray}
In energy-momentum Appendix, for the first identity we just need to remember $N$ of \ref{chfiridentity0} is a free $(n-4)$-form, then we can find the first identity
\begin{equation}
d*(* F\wedge g_{\beta\lambda}dx^\lambda)
=-\partial_\beta F\qquad\qed\label{engfirstidentity}
\end{equation}
For the second identity, after lines of algebra similar to \ref{calcexample1} one can show
\begin{eqnarray}
*d*(F\wedge dx^\beta)
&=-\frac{1}{4}(4\partial_\alpha F^{\alpha\mu} g^{\nu\beta} + 2\partial_\alpha F^{\mu\nu} g^{\alpha\beta})g_{\mu\gamma}g_{\nu\theta}dx^\gamma \wedge dx^\theta\nonumber\\
&=4\pi J\wedge dx^\beta-\partial_\alpha F g^{\alpha\beta}\qquad\qed\label{engsecidentity}
\end{eqnarray}
To prove that componants of \ref{emconservation} are the same as those of \ref{energym} we have
\begin{equation}
dx^\phi\wedge*(*F\wedge dx^\alpha)\wedge*F
=\epsilon^{\phi\lambda\mu^{n-2}}F_{\lambda\gamma}g^{\gamma\alpha}\epsilon_{\underrightarrow{\nu^2}\underrightarrow{\mu^{n-2}}}F^{\nu^2}vol^n
=-F^{\phi\lambda}F^\alpha_{\ \lambda}vol^n\nonumber
\end{equation}
and
\begin{eqnarray}
dx^\phi\wedge F\wedge*(F\wedge dx^\alpha)
&=\epsilon^{\phi\nu^2\mu^{n-3}}F_{\underrightarrow{\nu^2}}\epsilon_{\underrightarrow{\sigma^2}\lambda\underrightarrow{\mu^{n-3}}}F^{\sigma^2}g^{\lambda\alpha}vol^n\nonumber\\
&=\frac{1}{4}(2F_{\nu^2}F^{\nu^2}g^{\phi\alpha}+4F_{\nu^2}F^{\phi\nu_1}g^{\nu_2\alpha})vol^n\nonumber\\
&=\left(\frac{1}{2}F_{\nu^2}F^{\nu^2}g^{\phi\alpha}-F^{\phi\lambda}F^\alpha_{\ \lambda}\right)vol^n\nonumber
\end{eqnarray}
so
\begin{eqnarray}
T^{\alpha\phi}vol^n &:=dx^\phi\wedge T^\alpha
=-\frac{1}{2}dx^\phi\wedge\Big(*(*F\wedge dx^\alpha)\wedge*F
+F\wedge *(F\wedge dx^\alpha)\Big)\nonumber\\
&=\left(F^{\phi\lambda}F^\alpha_{\ \lambda}-\frac{1}{4}F_{\nu^2}F^{\nu^2}g^{\phi\alpha}\right)vol^n\qquad\qed\label{energym0}
\end{eqnarray}
which is the same as \ref{energym}.


\end{document}
